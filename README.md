## Remote Farm IoT with Animal detector

The projects detects conditions on the farm and alerts over WhatsApp any anomaly including if any animal is detected on farm.

### Anomaly Conditions are: 
1.  Moisture less than threshold
2.  Temperature less than threshold
3.  Pressure less than threshold
4.  Presence of Animal on the farm

![project-image](https://ars.els-cdn.com/content/image/1-s2.0-S0168169919322562-gr1.jpg)

**[Image Credits to rightful owner of the image.](https://doi.org/10.1016/j.compag.2019.105150)**

## Shunya Stack
Project is built by using the Shunya stack.

-   ShunyaOS is a lightweight Operating system with built-in support for AI and IOT.
-   Shunya Stack Low Code platform to build AI, IoT and AIoT products, that allows you to rapidly create and deploy AIoT products with ease.

For more information on ShunyaOS see : http://demo.shunyaos.org


## Documentation
For developers see detailed Documentation on the components of the project in the [Wiki](https://gitlab.com/iotiotdotin/community-projects/remote-farm-iot-with-animal-detector/-/wikis/home)

## Project Overview 

1.  [Project Plan Excel](https://docs.google.com/spreadsheets/d/1JoyyVkEdNLjNapWaNIxI-_rHxf5zABudU69Pt41rKvg/edit#gid=0)


## Contributing
Help us improve the project.

Ways you can help:

1.  Choose from the existing issue and work on those issues.
2.  Feel like the project could use a new feature, make an issue to discuss how it can be implemented and work on it.
3.  Find a bug create an Issue and report it.
4.  Review Issues or Merge Requests, give the developers the feedback.
5.  Fix Documentation.

## Contributors 

#### Team Lead 
1. Shunya OS (@shunyaos)

#### Active Contributors.

1.  Member1 - Yogesh (yogesh@iotiot.in)
2.  Member2 - Sneha (sneha@iotiot.in)
