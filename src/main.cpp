/**
 * @file main.cpp
 * @brief Template code for building with Shunya Stack
 *
 * Compilation: Code comes with a cmake file, just run cmake
 *
 * Usage : Just run the command './main'
 */

/* --- Standard Includes --- */
#include <iostream>
#include <cstdlib>
#include <stdint.h>
#include <time.h>
#include <unistd.h>
#include <string>
#include <errno.h>

#include <opencv2/opencv.hpp>


/* --- RapidJSON Includes --- */
/* MANDATORY: Allows to parse Shunya AI binaries output */
#include "rapidjson/document.h"
#include "rapidjson/writer.h"
#include "rapidjson/istreamwrapper.h"
#include "rapidjson/stringbuffer.h"
#include "rapidjson/ostreamwrapper.h"
#include "rapidjson/filereadstream.h"

#include "subprocess.hpp" /* MANDATORY: Allows to run Shunya AI binaries */
#include "exutils.h" /* MANDATORY: Allows to parse Shunya AI binaries output */


/* --- Shunya Interfaces Includes --- */
#include <si/shunyaInterfaces.h> /* MANDATORY: Contains all IoT Functions */
#include <si/video.h>
#include <si/whatsapp.h> 

#define MOISTURE_THRESHOLD 10
#define TEMPERATURE_THRESHOLD_MIN_C 20
#define TEMPERATURE_THRESHOLD_MAX_C 45
#define PRESSURE_THRESHOLD_kPa 101
#define TEMPERATURE_THRESHOLD_MIN TEMPERATURE_THRESHOLD_MIN_C
#define TEMPERATURE_THRESHOLD_MAX TEMPERATURE_THRESHOLD_MAX_C
#define PRESSURE_THRESHOLD PRESSURE_THRESHOLD_kPa


using namespace std;
using namespace rapidjson;

/* Whatsapp settings variable */
static whatsappObj whatsapp;

void sendAlert(std::string message)
{
    int16_t rc = sendWhatsappTxtMessage(whatsapp, message.c_str());

    if (rc < 0) {
        fprintf(stderr, "ERROR: failed to send whatsapp message.");
    }
}

int main(void)
{
    /* MANDATORY: Initializes the Shunya components */
    initLib();
    captureObj cam = newCaptureDevice("video-source"); /* Create capture object */
    cv::Mat frame; /* Variable to store frame */
    /* Load the Whatsapp settings */
    whatsapp = newWhatsapp("whatsappServer");

    while (1) {
        bool animalPresentInFrame = false;
        /* Write your code here */
        int16_t moisture = getAdc(1);
        int16_t temperature = getAdc(2);
        int16_t pressure = getAdc(3);
        /*################## Call Video Component functions ################*/
        frame  = captureFrameToMem(&cam); /* Capture one frame at a time in a loop*/

        if (frame.empty()) {
            fprintf(stderr, "End of video file!.");
            closeCapture(&cam);
            return 0;
        }

        /* ---- Create cv::mat image to base64 string ---- */
        std::string b64InpImg = mat2Base64(frame);
        /* Set value to 0.8 i.e (80/100) to set the probability to 80% or higher */
        float probability = 0.8;
        /* ---- Create Input JSON ---- */
        rapidjson::Document inputJson;
        inputJson.SetObject();
        rapidjson::Value inpImage;
        /* Call API binary */
        inpImage.SetString(b64InpImg.c_str(), strlen(b64InpImg.c_str()), inputJson.GetAllocator());
        inputJson.AddMember("inputImage", inpImage, inputJson.GetAllocator());
        inputJson.AddMember("probability", probability, inputJson.GetAllocator());
        /*############### Call detectObjects Component ##################*/
        subprocess::popen detectObjects("/usr/bin/detectObjects", {});
        detectObjects.stdin() << jsonDoc2Text(inputJson) << std::endl;
        detectObjects.close();
        std::string detectObjectsOut;
        detectObjects.stdout() >> detectObjectsOut;
        rapidjson::Document detectObjectsJson = readJsonString(detectObjectsOut);

        if (detectObjectsJson.HasMember("data")) {
            rapidjson::Value &results = detectObjectsJson["data"]["objects"];
            assert(results.IsArray());

            /* Reading from json file and add it in the structure */
            for (rapidjson::SizeType i = 0; i < results.Size(); i++) {
                std::string objLabel = results[i]["object"].GetString();

                if ((objLabel.compare("cow") == 0) ||
                        (objLabel.compare("horse") == 0) ||
                        (objLabel.compare("cat") == 0) ||
                        (objLabel.compare("dog") == 0) ||
                        (objLabel.compare("sheep") == 0) ) {
                    animalPresentInFrame = true;
                }
            }

        } else {
            std::cout<<"No json member as \"data\""<<std::endl;
        }

        if (moisture < MOISTURE_THRESHOLD) {
            sendAlert("ALERT! Mositure is lower than threshold.");
        }

        if (pressure < PRESSURE_THRESHOLD) {
            sendAlert("ALERT! pressure is lower than threshold.");
        }

        if (temperature > TEMPERATURE_THRESHOLD_MAX) {
            sendAlert("ALERT! temperature is higher than threshold.");
        }

        if (temperature < TEMPERATURE_THRESHOLD_MIN) {
            sendAlert("ALERT! temperature is lower than threshold.");
        }

        if (animalPresentInFrame) {
            sendAlert("ALERT! Animals detected on Farm.");
        }
    }

    return 0;
}